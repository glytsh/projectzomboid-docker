# projectzomboid-docker

Ce projet a pour but de déployer un serveur Project Zomboid grâce a Docker-compose 

Project Zomboid est un jeu Survival horror en coop.

## Qu'est ce qui est déployé ? 

Projet va deployer 3 containers: 
- LinuxGSM, c'est ce qui va permettre de déployer le serveur sur steamDB et mettre en ligne tout ce dont on a besoin
- Prometheus, afin de surveiller l'état du serveur prometheus transmettra les informations au Grafana Cloud
- NodeExporter, qui sera l'exporter de data

## Comment gérer le serveur ?

Le serveur est réglable directement dans les fichiers de configs se trouvant dans :
`config-lgsm/pzserver/common`

sinon vous pouvez également gérer le serveur dans le container grâce a la commande : 
`docker exec -it pz-server /bin/bash`

ensuite dans le container vous pourez avoir accès à la commande 
```
./pzserver #celle-ci vous donnera accès a ces options 



Commands
start         st   | Start the server.
stop          sp   | Stop the server.
restart       r    | Restart the server.
monitor       m    | Check server status and restart if crashed.
test-alert    ta   | Send a test alert.
details       dt   | Display server information.
postdetails   pd   | Post details to termbin.com (removing passwords).
skeleton      sk   | Create a skeleton directory.
update-lgsm   ul   | Check and apply any LinuxGSM updates.
update        u    | Check and apply any server updates.
force-update  fu   | Apply server updates bypassing check.
validate      v    | Validate server files with SteamCMD.
check-update  cu   | Check if a gameserver update is available
backup        b    | Create backup archives of the server.
console       c    | Access server console.
debug         d    | Start server directly in your terminal.
send          sd   | Send command to game server console.
install       i    | Install the server.
auto-install  ai   | Install the server without prompts.
developer     dev  | Enable developer Mode.
donate        do   | Donation options.
```

## Monitoring 

Le monitoring se configure dans le prometheus.yml. Vous devez y ajouter votre clée API grafana afin d'avoir accès a l'état de votre serveur.