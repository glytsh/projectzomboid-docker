function SpawnPoints()
return {
  chef = {
    { worldX = 21, worldY = 18, posX = 227, posY = 31, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 115, posY = 231, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 205, posY = 270, posZ = 0 },
    { worldX = 22, worldY = 18, posX = 159, posY = 87, posZ = 0 },
    { worldX = 20, worldY = 21, posX = 179, posY = 73, posZ = 0 },
    { worldX = 21, worldY = 17, posX = 71, posY = 168, posZ = 0 }
  },
  constructionworker = {
    { worldX = 21, worldY = 17, posX = 71, posY = 168, posZ = 0 },
    { worldX = 17, worldY = 20, posX = 295, posY = 56, posZ = 0 },
    { worldX = 17, worldY = 20, posX = 233, posY = 63, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 136, posY = 143, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 71, posY = 223, posZ = 0 },
    { worldX = 19, worldY = 18, posX = 260, posY = 23, posZ = 0 }
  },
  doctor = {
    { worldX = 21, worldY = 18, posX = 227, posY = 31, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 115, posY = 231, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 205, posY = 270, posZ = 0 },
    { worldX = 22, worldY = 18, posX = 159, posY = 87, posZ = 0 },
    { worldX = 21, worldY = 17, posX = 172, posY = 171, posZ = 0 }
  },
  fireofficer = {
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 282, posY = 49, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 219, posY = 79, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 227, posY = 31, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 115, posY = 231, posZ = 0 }
  },
  parkranger = {
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 282, posY = 49, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 219, posY = 79, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 227, posY = 31, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 115, posY = 231, posZ = 0 }
  },
  policeofficer = {
    { worldX = 21, worldY = 18, posX = 219, posY = 79, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 114, posY = 216, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 79, posY = 258, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 284, posY = 106, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 282, posY = 49, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 219, posY = 79, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 227, posY = 31, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 205, posY = 270, posZ = 0 },
    { worldX = 21, worldY = 17, posX = 71, posY = 168, posZ = 0 }
  },
  repairman = {
    { worldX = 17, worldY = 20, posX = 295, posY = 56, posZ = 0 },
    { worldX = 17, worldY = 20, posX = 233, posY = 63, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 114, posY = 216, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 79, posY = 258, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 19, worldY = 18, posX = 260, posY = 23, posZ = 0 },
    { worldX = 22, worldY = 17, posX = 136, posY = 143, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 71, posY = 223, posZ = 0 }
  },
  securityguard = {
    { worldX = 17, worldY = 20, posX = 295, posY = 56, posZ = 0 },
    { worldX = 17, worldY = 20, posX = 233, posY = 63, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 114, posY = 216, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 79, posY = 258, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 19, worldY = 18, posX = 260, posY = 23, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 284, posY = 106, posZ = 0 },
    { worldX = 20, worldY = 18, posX = 282, posY = 49, posZ = 0 }
  },
  unemployed = {
    { worldX = 17, worldY = 20, posX = 295, posY = 56, posZ = 0 },
    { worldX = 17, worldY = 20, posX = 233, posY = 63, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 57, posY = 249, posZ = 0 },
    { worldX = 20, worldY = 17, posX = 266, posY = 228, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 114, posY = 216, posZ = 0 },
    { worldX = 19, worldY = 17, posX = 79, posY = 258, posZ = 0 },
    { worldX = 21, worldY = 18, posX = 51, posY = 20, posZ = 0 },
    { worldX = 19, worldY = 18, posX = 260, posY = 23, posZ = 0 }
  }
}
end
