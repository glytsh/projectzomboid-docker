function SpawnPoints()
return {
  chef = {
    { worldX = 26, worldY = 38, posX = 285, posY = 112, posZ = 0 },
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 26, worldY = 38, posX = 203, posY = 145, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  constructionworker = {
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  doctor = {
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 26, worldY = 38, posX = 203, posY = 145, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 27, posY = 222, posZ = 0 }
  },
  fireofficer = {
    { worldX = 27, worldY = 39, posX = 44, posY = 39, posZ = 0 },
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 26, worldY = 38, posX = 203, posY = 145, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  parkranger = {
    { worldX = 27, worldY = 39, posX = 242, posY = 49, posZ = 0 },
    { worldX = 28, worldY = 39, posX = 41, posY = 111, posZ = 0 },
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  policeofficer = {
    { worldX = 26, worldY = 39, posX = 273, posY = 30, posZ = 0 },
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  repairman = {
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  securityguard = {
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  },
  unemployed = {
    { worldX = 26, worldY = 39, posX = 274, posY = 31, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 108, posY = 200, posZ = 0 },
    { worldX = 27, worldY = 38, posX = 146, posY = 270, posZ = 0 },
    { worldX = 27, worldY = 39, posX = 227, posY = 114, posZ = 0 },
    { worldX = 28, worldY = 38, posX = 18, posY = 286, posZ = 0 }
  }
}
end
