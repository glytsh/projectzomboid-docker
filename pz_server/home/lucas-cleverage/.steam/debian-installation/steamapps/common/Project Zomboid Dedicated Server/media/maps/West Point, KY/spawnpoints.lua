function SpawnPoints()
return {
  chef = {
    { worldX = 40, worldY = 23, posX = 77, posY = 180, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 255, posY = 184, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 282, posY = 206, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 203, posY = 265, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 204, posY = 125, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 137, posY = 86, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 }
  },
  constructionworker = {
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 62, posY = 15, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 137, posY = 86, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 },
    { worldX = 37, worldY = 22, posX = 197, posY = 130, posZ = 0 }
  },
  doctor = {
    { worldX = 39, worldY = 22, posX = 235, posY = 213, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 235, posY = 213, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 235, posY = 213, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 235, posY = 213, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 72, posY = 152, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 133, posY = 24, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 240, posY = 277, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 }
  },
  fireofficer = {
    { worldX = 39, worldY = 22, posX = 72, posY = 152, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 133, posY = 24, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 240, posY = 277, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 }
  },
  parkranger = {
    { worldX = 39, worldY = 22, posX = 72, posY = 152, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 240, posY = 277, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 },
    { worldX = 37, worldY = 22, posX = 267, posY = 275, posZ = 0 }
  },
  policeofficer = {
    { worldX = 39, worldY = 22, posX = 246, posY = 285, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 72, posY = 152, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 206, posY = 16, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 208, posY = 49, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 133, posY = 24, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 240, posY = 277, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 }
  },
  repairman = {
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 62, posY = 15, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 137, posY = 86, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 },
    { worldX = 37, worldY = 22, posX = 197, posY = 130, posZ = 0 }
  },
  securityguard = {
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 137, posY = 86, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 240, posY = 277, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 },
    { worldX = 37, worldY = 22, posX = 197, posY = 130, posZ = 0 }
  },
  unemployed = {
    { worldX = 39, worldY = 22, posX = 204, posY = 125, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 59, posY = 84, posZ = 0 },
    { worldX = 39, worldY = 22, posX = 92, posY = 186, posZ = 0 },
    { worldX = 39, worldY = 23, posX = 196, posY = 115, posZ = 0 },
    { worldX = 40, worldY = 23, posX = 21, posY = 14, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 137, posY = 86, posZ = 0 },
    { worldX = 38, worldY = 23, posX = 80, posY = 124, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 243, posY = 96, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 94, posY = 102, posZ = 0 },
    { worldX = 38, worldY = 22, posX = 12, posY = 124, posZ = 0 },
    { worldX = 37, worldY = 22, posX = 197, posY = 130, posZ = 0 }
  }
}
end
